function TestServerCallbackFunction(src, ~)
%TestServerCallbackFunction Called when number of bytes is available in buffer
%   Detailed explanation goes here
    fprintf("Received message!\n");
    MessageLength = read(src, 4, 'string');
    MessageLength = double(MessageLength);
    MessageContent = read(src, MessageLength, 'string');
    MessageContent = char(MessageContent);
    MessageHeader = MessageContent(1:3);
    MessageHeader = double(string(MessageHeader));
    MessageBody = MessageContent(4:MessageLength);

    MessageBody = string(MessageBody);
    fprintf("%s\n", MessageBody);
    toc   
end