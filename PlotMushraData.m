%PlotMushraData - Plots result in line graph
%Display settings can be changed in ServerConfig.ini


%read settings
IniChecker = IniConfig();
IniChecker.ReadFile('ServerConfig.ini');
sections = IniChecker.GetSections();
[ServerKeys, ~] = IniChecker.GetKeys(sections{1});
ServerKeys = IniChecker.GetValues(sections{1}, ServerKeys);
ServerSettings = table(string(ServerKeys{1}), ServerKeys{2}, string(ServerKeys{3}), ServerKeys{4}, ServerKeys{5}, string(ServerKeys{6}), 'VariableNames', ...
    {'IPAddress', 'PortNumber', 'HeadTrackingFile','HeadTrackingEnabled', 'ServerMode', 'ExperimentDirectory'});
[MushraKeys, ~] = IniChecker.GetKeys(sections{2});
MushraKeys = IniChecker.GetValues(sections{2}, MushraKeys);
MushraSettings = table(MushraKeys{1}, MushraKeys{2}, MushraKeys{3}, MushraKeys{4}, string(MushraKeys{5}), string(MushraKeys{6}), MushraKeys{7}, ServerKeys{4}, ...
    'VariableNames',  {'SliderStartValues', 'ResetOnAttributeChange', 'DiscreteSliders', 'Stepsize', 'Attributes', 'LevelNames', 'Randomize', 'HeadTrackingEnabled'});
Levels = split(MushraSettings.LevelNames, ",");
[DisplayKeys, ~] = IniChecker.GetKeys(sections{3});
DisplayKeys = IniChecker.GetValues(sections{3}, DisplayKeys);
DisplaySettings = table(string(DisplayKeys{1}), string(DisplayKeys{2}), string(DisplayKeys{3}), DisplayKeys{4},'VariableNames', ...
    {'Color1', 'Color2', 'Color3', 'Normalize'});

%go through subdirectories
CurrentDirectory = pwd + "\" + ServerSettings.ExperimentDirectory;
S = dir(fullfile(CurrentDirectory, '*'));
N = setdiff({S([S.isdir]).name}, {'.', '..'});
Level1Values = [];
Level2Values = [];
Level3Values = [];

attributes = "";
for i = 1:numel(N)
    T = readtable(CurrentDirectory + "\" + N(i) + "\" + N(i) + ".csv", 'delimiter', ',');
    C = readcell(CurrentDirectory + "\" + N(i) + "\" + "Attributes.txt", 'delimiter', ',');

    %assign sliders to levels
    LevelASubTable = T(find(strcmp(T.LevelState, "A")),:);
    if height(LevelASubTable(find(strcmp(LevelASubTable.CurrentLevel, Levels(1))),:)) ~= 0
        SliderALevel = Levels(1);
    elseif height(LevelASubTable(find(strcmp(LevelASubTable.CurrentLevel, Levels(2))),:)) ~= 0
        SliderALevel = Levels(2);
    elseif height(LevelASubTable(find(strcmp(LevelASubTable.CurrentLevel, Levels(3))),:)) ~= 0
        SliderALevel = Levels(3);
    end

    LevelBSubTable = T(find(strcmp(T.LevelState, "B")),:);
    if height(LevelBSubTable(find(strcmp(LevelBSubTable.CurrentLevel, Levels(1))),:)) ~= 0
        SliderBLevel = Levels(1);
    elseif height(LevelBSubTable(find(strcmp(LevelBSubTable.CurrentLevel, Levels(2))),:)) ~= 0
        SliderBLevel = Levels(2);
    elseif height(LevelBSubTable(find(strcmp(LevelBSubTable.CurrentLevel, Levels(3))),:)) ~= 0
        SliderBLevel = Levels(3);
    end

    LevelCSubTable = T(find(strcmp(T.LevelState, "C")),:);
    if height(LevelCSubTable(find(strcmp(LevelCSubTable.CurrentLevel, Levels(1))),:)) ~= 0
        SliderCLevel = Levels(1);
    elseif height(LevelCSubTable(find(strcmp(LevelCSubTable.CurrentLevel, Levels(2))),:)) ~= 0
        SliderCLevel = Levels(2);
    elseif height(LevelCSubTable(find(strcmp(LevelCSubTable.CurrentLevel, Levels(3))),:)) ~= 0
        SliderCLevel = Levels(3);
    end

    for j = 1:height(C) %for each attribute
        CurrentAttribute = string(C{j});
        attributes(end+1) = CurrentAttribute;
        CurrentAttributeSubTable = T(find(strcmp(T.CurrentAttribute, CurrentAttribute)),:);

        SliderASubTable = CurrentAttributeSubTable(find(strcmp(CurrentAttributeSubTable.SliderNumber, "A")),:);
        try
            SliderAValue = SliderASubTable(end,:).Value;
        catch
            SliderAValue = 0;
        end
        if strcmp(SliderALevel, Levels(1))
            Level1Values(end+1) = SliderAValue;
        elseif strcmp(SliderALevel, Levels(2))
            Level2Values(end+1) = SliderAValue;
        elseif strcmp(SliderALevel, Levels(3))
            Level3Values(end+1) = SliderAValue;
        end

        SliderBSubTable = CurrentAttributeSubTable(find(strcmp(CurrentAttributeSubTable.SliderNumber, "B")),:);
        try
            SliderBValue = SliderBSubTable(end,:).Value;
        catch
            SliderBValue = 0;
        end
        if strcmp(SliderBLevel, Levels(1))
            Level1Values(end+1) = SliderBValue;
        elseif strcmp(SliderBLevel, Levels(2))
            Level2Values(end+1) = SliderBValue;
        elseif strcmp(SliderBLevel, Levels(3))
            Level3Values(end+1) = SliderBValue;
        end

        SliderCSubTable = CurrentAttributeSubTable(find(strcmp(CurrentAttributeSubTable.SliderNumber, "C")),:);
        try
            SliderCValue = SliderCSubTable(end,:).Value;
        catch 
            SliderCValue = 0;
        end
        if strcmp(SliderCLevel, Levels(1))
            Level1Values(end+1) = SliderCValue;
        elseif strcmp(SliderCLevel, Levels(2))
            Level2Values(end+1) = SliderCValue;
        elseif strcmp(SliderCLevel, Levels(3))
            Level3Values(end+1) = SliderCValue;
        end
    end
end

attributes = attributes(2:end);
ValueTable = table(transpose(attributes), transpose(Level1Values), transpose(Level2Values), transpose(Level3Values));
ValueTable.Properties.VariableNames = {'Attribute', 'Level1', 'Level2', 'Level3'};

VisualValueTable = table('Size', [0 4], 'VariableTypes', {'string', 'double', 'double', 'double'}, 'VariableNames', {'Attribute', 'Level1', 'Level2', 'Level3'});
AudioValueTable = table('Size', [0 4], 'VariableTypes', {'string', 'double', 'double', 'double'}, 'VariableNames', {'Attribute', 'Level1', 'Level2', 'Level3'});

rows = height(ValueTable);
for row=1:rows
    attr = char(ValueTable(row,:).Attribute);
    newAttr = string(attr(1:end-4));
    if attr(end) == 'V'
        VisualValueTable(end+1,:) = ValueTable(row,:);
        VisualValueTable(end,:).Attribute = newAttr;
    elseif attr(end) == 'A'
        AudioValueTable(end+1,:) = ValueTable(row,:);
        AudioValueTable(end,:).Attribute = newAttr;
    else
        VisualValueTable(end+1,:) = ValueTable(row,:);
        VisualValueTable(end,:).Attribute = newAttr;
    end
end
VisualValueTable = sortrows(VisualValueTable, "Attribute");
AudioValueTable = sortrows(AudioValueTable, "Attribute");

%find duplicates
VCount = 0;
ACount = 0;
LastVAttr = "";
LastAAttr = "";
CurrentAttribute = "";
VTickLabels = strings(0);
ATickLabels = strings(0);

for row=1:height(VisualValueTable) %visual
    CurrentAttribute = VisualValueTable(row,:).Attribute;
    if strcmp(CurrentAttribute, LastVAttr)
        VCount = VCount+1;
        VisualValueTable(row,:).Attribute = CurrentAttribute + " " + VCount;
    else
        VCount = 0;
    end
    VTickLabels(end+1) = CurrentAttribute;
    LastVAttr = CurrentAttribute;
end

for row=1:height(AudioValueTable) %audio
    CurrentAttribute = AudioValueTable(row,:).Attribute;
    if strcmp(CurrentAttribute, LastAAttr)
        ACount = ACount+1;
        AudioValueTable(row,:).Attribute = CurrentAttribute + " " + ACount;
    else
        ACount = 0;
    end
    ATickLabels(end+1) = CurrentAttribute;
    LastAAttr = CurrentAttribute;
end

%normalize
for row=1:height(VisualValueTable)
    VisualValueTable{row,2:4} = VisualValueTable{row,2:4}*10;
end
for row=1:height(AudioValueTable)
    AudioValueTable{row,2:4} = AudioValueTable{row,2:4}*10;
end

if(DisplaySettings.Normalize)
    for row=1:height(VisualValueTable)
        MaxValue = max(VisualValueTable{row, 2:4}, [], 2);
        VisualValueTable{row, 2:4} = VisualValueTable{row, 2:4}/MaxValue;
    end
    for row=1:height(AudioValueTable)
        MaxValue = max(AudioValueTable{row, 2:4}, [], 2);
        AudioValueTable{row, 2:4} = AudioValueTable{row, 2:4}/MaxValue;
    end
end

%plot
t = tiledlayout(1,2,'TileSpacing', 'compact');

ax1 = nexttile;
plot(categorical(VisualValueTable.Attribute), VisualValueTable.Level1, 'LineStyle', '--', 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 35, 'Color', char(DisplaySettings.Color1));
hold on
plot(categorical(VisualValueTable.Attribute), VisualValueTable.Level2, 'LineStyle', '--', 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 35, 'Color', char(DisplaySettings.Color2));
hold on
plot(categorical(VisualValueTable.Attribute), VisualValueTable.Level3, 'LineStyle', '--', 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 35, 'Color', char(DisplaySettings.Color3));
xticklabels(VTickLabels);
xlabel('Attribute', 'FontSize', 12);
ylabel('Score', 'FontSize', 12);
grid on
title('Visual', 'FontSize', 12);
legend(char(Levels(1)), char(Levels(2)), char(Levels(3)), 'FontSize', 13);

ax2 = nexttile;
plot(categorical(AudioValueTable.Attribute), AudioValueTable.Level1, 'LineStyle', '--', 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 35, 'Color', char(DisplaySettings.Color1));
hold on
plot(categorical(AudioValueTable.Attribute), AudioValueTable.Level2, 'LineStyle', '--', 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 35, 'Color', char(DisplaySettings.Color2));
hold on
plot(categorical(AudioValueTable.Attribute), AudioValueTable.Level3, 'LineStyle', '--', 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 35, 'Color', char(DisplaySettings.Color3));
xticklabels(ATickLabels);
xlabel('Attribute', 'FontSize', 12);
ylabel('Score', 'FontSize', 12);
grid on
title('Auditory', 'FontSize', 12);
legend(char(Levels(1)), char(Levels(2)), char(Levels(3)), 'FontSize', 13);