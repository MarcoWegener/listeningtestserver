%TestClient - This script can be used to check if the server configuration
%is working.
%Creates a server and a client, sends a message to the server and stops the
%elapsed time. Use ServerMode 0.

fprintf("##############\n");
fprintf("Testing server\n");
fprintf("##############\n\n");

RunTcpServer;
fprintf("Creating client...\n\n");
Client = tcpclient("127.0.0.1", 4000);
fprintf("Sending test message\n\n");
tic
write(Client, "0014001Hello World");