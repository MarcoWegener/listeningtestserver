function MushraServerCallbackFunction(src, ~, MushraSettings, ServerSettings)
%MushraServerCallbackFunction Called when number of bytes is available in buffer
%   Detailed explanation goes here

    global mushraTable
    global headtrackingTable
    global ExperimentID

    fprintf("##########################################\n");
    fprintf("Received Message\n");
    fprintf("##########################################\n\n");
    
    MessageLength = read(src, 4, 'string');
    MessageLength = double(MessageLength);
    MessageContent = read(src, MessageLength, 'string');
    MessageContent = char(MessageContent);
    MessageHeader = MessageContent(1:3);
    MessageHeader = double(string(MessageHeader));
    MessageBody = MessageContent(4:MessageLength);

    switch MessageHeader
        case 1
        case 2 %slider data
            fprintf("Slider data received.\n\n");
            TransformData(MessageBody, "MushraSlider");
            disp(mushraTable);
        case 11 %no hmd warning
            fprintf("Warning: No HMD detected/enabled.\n\n");
        case 12 %headtracking written
        case 13 %headtracking speed
        case 14
        case 15 %finish
            fprintf("Finishing session...\n");
            if MessageBody(1) == '1'
                %read headtracking file
                %IniChecker = IniConfig();
                %IniChecker.ReadFile('ServerConfig.ini');
                %sections = IniChecker.GetSections();
                %[ServerKeys, ~] = IniChecker.GetKeys(sections{1});
                %ServerKeys = IniChecker.GetValues(sections{1}, ServerKeys);
                headtrackingTable = readtable(ServerSettings.HeadTrackingFile, 'Delimiter', ' ', 'VariableNamingRule', 'preserve');
                headtrackingTable.Properties.VariableNames = { 'Pitch', 'Yaw', 'Rotation', 'X', 'Y', 'Z' };
            end
            fprintf("Writing datta...\n");
            %save(ServerSettings.ExperimentDirectory + "\" + "test");
            writetable(mushraTable, ServerSettings.ExperimentDirectory + "\" + ExperimentID  + "\" + ExperimentID + ".csv", 'WriteVariableNames', 1);
            writetable(headtrackingTable, ServerSettings.ExperimentDirectory + "\" + ExperimentID + "\" + "headtracking.csv", 'Delimiter', ' ', 'WriteVariableNames', 1);
            
            attributes = split(MushraSettings.Attributes, ",");
            fid = fopen(ServerSettings.ExperimentDirectory + "\" + ExperimentID + "\" + "Attributes.txt", "wt");
            for n = 1:length(attributes)
                fprintf(fid, '%s\n', attributes(n));
            end
            fclose(fid);
            fprintf("Clearing head tracking file...\n\n");
            delete(ServerSettings.HeadTrackingFile);
            fid = fopen(ServerSettings.HeadTrackingFile, 'w');
            fclose(fid);
            ExperimentID = "";
        otherwise
            fprintf("Message could not be processed!\n");
            time = MessageBody(6:MessageLength);
            disp(typecast(uint8(time), 'single'));
    end
end