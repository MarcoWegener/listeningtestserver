function MushraServerConnectedFunction(src, ~, MushraSettings, ServerSettings)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    
    global ExperimentID
    
    %Client connected
    if src.Connected 
        
        %is web server running?
        fprintf("Checking Web connection...\n\n");
        if ServerSettings.WebServerRunning
            fprintf("Web server running...\n");
            
            %****************
            %****DO STUFF****
            %****************

        %not running -> enter ID manually
        else
            while 1
                fprintf("Web server not running.\n");
                ExperimentID = input("Enter experiment ID: ", "s");

                if ExperimentID == ""
                    fprintf("Experiment ID cannot be empty!\n");
                    continue
                else
                    break
                end
            end
        end

        fprintf("Connection established. Sending startup data...\n\n");

        %Send startup data
        OpCode = uint8(3);
        SliderAStartValue = string(num2str(single(MushraSettings.SliderStartValues(1)), '%.2f'));
        SliderBStartValue = string(num2str(single(MushraSettings.SliderStartValues(2)), '%.2f'));
        SliderCStartValue = string(num2str(single(MushraSettings.SliderStartValues(3)), '%.2f'));
        SliderStartValues = SliderAStartValue + SliderBStartValue + SliderCStartValue;
        ResetSliders = string(MushraSettings.ResetOnAttributeChange);
        DiscreteSliders = string(MushraSettings.DiscreteSliders);
        Attributes = MushraSettings.Attributes;
        AttributesSize = strlength(Attributes);
        AttributesLength = string(AttributesSize);
        HeadTrackingEnabled = string(MushraSettings.HeadTrackingEnabled);

        switch strlength(AttributesLength)
            case 1
                zerofill = "000";
            case 2
                zerofill = "00";
            case 3
                zerofill = "0";
            case 4
                zerofill = "";
        end
        AttributesLength = zerofill + AttributesLength;

        LevelNames = MushraSettings.LevelNames;
        LevelNamesSize = strlength(LevelNames);
        LevelNamesLength = string(LevelNamesSize);

        switch strlength(LevelNamesLength)
            case 1
                zerofill = "000";
            case 2
                zerofill = "00";
            case 3
                zerofill = "0";
            case 4
                zerofill = "";
        end
        LevelNamesLength = zerofill + LevelNamesLength;

        Randomize = string(MushraSettings.Randomize);

        MessageContent = SliderStartValues + ResetSliders + DiscreteSliders + AttributesLength + Attributes + LevelNamesLength + LevelNames + Randomize + HeadTrackingEnabled;
        MessageContent = uint8(char(MessageContent));
        Message = cat(2, OpCode, MessageContent);
        MessageSize = size(Message);
        MessageLength = uint8(MessageSize(2));
        Message = cat(2, MessageLength, Message);
        write(src, Message, 'uint8');

        %create folder
        [status, msg, msgID] = mkdir(pwd + "\" + ServerSettings.ExperimentDirectory, ExperimentID);
        if status == 1
            if strcmp(msgID, 'MATLAB:MKDIR:DirectoryExists')
                fprintf("Directory already exists.\n\n");
            end
        else
            error("Failed to create subfolder.\n\n");
        end
    else
        flush(src);
        fprintf("Connection closed.\n");
    end
end