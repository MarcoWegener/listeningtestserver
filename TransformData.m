function TransformData(data, datatype)
%TransformData Transforms data from char array specified by datatype input
%   Detailed explanation goes here
    arguments
        data (1,:) char
        datatype (1,:) char {mustBeMember(datatype, {'MushraSlider', 'MushraFinish'})} = 'MushraSlider'
    end

    global mushraTable
    
    if (strcmp(datatype, 'MushraSlider'))
        %SliderNr = data(1);
        %LevelState = data(2);
        %CurrentLevelNum = double(string(data(3:6)));
        %CurrentLevel = string(data(7:7+CurrentLevelNum));
        %StartValue = typecast(uint8(data(7:10)), 'single');
        %Value = typecast(uint8(data(11:14)), 'single');
        %Discrete = uint8(data(15)) - 48;
        %Reset = uint8(data(16)) - 48;
        %Time = typecast(uint8(data(16:19)), 'single');
        SliderNr = data(1);
        LevelState = data(2);
        C = double(string(data(3:6)));
        CurrentLevel = string(data(7:7+C-1));
        A = double(string(data(7+C:7+C+3)));
        CurrentAttribute = string(data(7+C+4:7+C+4+A-1));
        StartValue = typecast(uint8(data(7+C+4+A:7+C+4+A+3)), 'single');
        Value = typecast(uint8(data(7+C+4+A+4: 7+C+4+A+7)), 'single');
        Discrete = uint8(data(7+C+4+A+8)) - 48;
        Reset = uint8(data(7+C+4+A+9)) - 48;
        Time = typecast(uint8(data(7+C+4+A+10: 7+C+4+A+13)), 'single');
        TimeMin = floor(Time / 60);
        TimeSec = mod(Time, 60);     
        t = cell(1,7);
        t{1,1} = string(SliderNr);
        t{1,2} = string(LevelState);
        t{1,3} = string(CurrentLevel);
        t{1,4} = string(CurrentAttribute);
        t{1,5} = StartValue;
        t{1,6} = Value;
        t{1,7} = logical(Discrete);
        t{1,8} = logical(Reset);
        t{1,9} = TimeMin;
        t{1,10} = TimeSec;
        mushraTable = [mushraTable;cell2table(t, ...
            "VariableNames", ["Slider Number", "Level State", "Current Level", "Current Attribute", "Start Value", ...
            "Value", "Discrete Steps", "Reset On Change", "Time Min", "Time Sec"])];
        return;
    end

end