# Listening Test Matlab Server



## Overview

TCP server implementation in Matlab to be used with [Unreal's ListeningTestUIPlugin](https://git.rwth-aachen.de/ihta/unreal/plugins/listening-test-ui). Requires [Instrument Control Toolbox](https://de.mathworks.com/products/instrument.html) to run.
The server can be used to configure listening experiments in VR and collet data during such experiments. Configuration is done by editing 'ServerConfig.ini'.

## Usage

1. Configure server settings in ServerConfig.ini

2. Start server using either

```
RunTcpServer.m
```

or from a command line using

```
.\RunTcpServer.bat
```

The server will listen for connections and automatically write results into desired location (default: ./experiments).

3. After experiments are done, you can plot data using either 'PlotMushraData.m' or with your own script.

## Configuration

Server settings can be configured in the *ServerConfig.ini* file:

### Server
- **sIPAddress:** the IP address to connect to
- **iPortNumber:** the port number to use
- **sHeadTrackingFile:** name of the .csv file to save head tracking data
- **bEnableHeadTracking:** wether or not head tracking is enabled
- **iServerMode:** 0 - Ping Test; 1 - Mushra Interface
- **sExperimentDirectory:** sub-directory to save experiment results
- **bWebServerRunning:** wether or not experiment is running remotely or locally

### Mushra
- **fSliderStartValues:** start values for three sliders
- **bResetSlidersOnAttributeChange:** wether or not sliders should reset to their start value when displaying a new attribute
- **bDiscreteSliders:** wether or not sliders should move in discrete steps
- **fDiscreteSliderStepSize:** step size of discrete sliders
- **sAttributes:** list of attributes to rate. Attributes are seperated by comma, without spaces
- **bRandomizeLevels:** wether or not level order should be randomized

### Display
- **sColor1, sColor2, sColor3:** Colors of data plots
- **bNormalizeValues:** wether or not data plots should be normalized