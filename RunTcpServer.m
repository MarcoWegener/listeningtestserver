%RunTcpServer - Creates a tcp server and listens for connections.
%Server settings are read from a config file 'ServerConfig.ini'. Requires
%Matlab's Instrument Control Toolbox to run.

%warning off;
fprintf("##########################################\n");
fprintf("Welcome. Setup is running...\n");
fprintf("##########################################\n\n");

%check if required packages are installed
if size(ver('instrument'), 1) == 0
    error('Instrument Control Toolbox seems to not be installed properly. Check Matlab installation.\n');
else
    fprintf("Found Instrument Control Toolbox.\n\n");
end

global mushraTable
global headtrackingTable
global ExperimentID
mushraTable = table();
headtrackingTable = table();
ExperimentID = "";


%read config file
fprintf("Reading settings from config file...\n\n");
IniChecker = IniConfig();
IniChecker.ReadFile('ServerConfig.ini');
sections = IniChecker.GetSections();
[ServerKeys, ~] = IniChecker.GetKeys(sections{1});
ServerKeys = IniChecker.GetValues(sections{1}, ServerKeys);
[MushraKeys, ~] = IniChecker.GetKeys(sections{2});
MushraKeys = IniChecker.GetValues(sections{2}, MushraKeys);
ServerSettings = table(string(ServerKeys{1}), ServerKeys{2}, string(ServerKeys{3}), ServerKeys{4}, ServerKeys{5}, string(ServerKeys{6}), ServerKeys{7}, 'VariableNames', ...
    {'IPAddress', 'PortNumber', 'HeadTrackingFile','HeadTrackingEnabled', 'ServerMode', 'ExperimentDirectory', 'WebServerRunning'});
MushraSettings = table(MushraKeys{1}, MushraKeys{2}, MushraKeys{3}, MushraKeys{4}, string(MushraKeys{5}), string(MushraKeys{6}), MushraKeys{7}, ServerKeys{4}, ...
    'VariableNames',  {'SliderStartValues', 'ResetOnAttributeChange', 'DiscreteSliders', 'Stepsize', 'Attributes', 'LevelNames', 'Randomize', 'HeadTrackingEnabled'});


[status, msg, msgID] = mkdir(pwd, ServerSettings.ExperimentDirectory);
if status == 1
    if strcmp(msgID, 'MATLAB:MKDIR:DirectoryExists')
        fprintf("Directory already exists.\n\n");
    end
else
    error("Failed to create experiment folder.\n\n");
end

%create server
fprintf("Creating server...\n\n");
try
    switch ServerSettings.ServerMode
        case 0 %test
            mushraTable = table;
            server = tcpserver(ServerSettings{1,1}, ServerSettings{1,2}, "ConnectionChangedFcn", @ServerConnectedFunction);
            configureCallback(server, "byte", 4, @TestServerCallbackFunction);
            flush(server);
        case 1 %mushra
            server = tcpserver(ServerSettings{1,1}, ServerSettings{1,2}, "ConnectionChangedFcn", ...
                @(src,eventdata)MushraServerConnectedFunction(src, eventdata, MushraSettings, ServerSettings));
            configureCallback(server, "byte", 4, @(src, eventdata)MushraServerCallbackFunction(src, eventdata, MushraSettings, ServerSettings));
            flush(server);
        otherwise
            server = 0;
            error('Failed to configure server callback.\n');
    end

    fprintf("##########################################\n");
    fprintf("Server running. Waiting for connections...\n");
    fprintf("Type 'clear all' to close the server.\n");
    fprintf("##########################################\n\n");
catch
    fprintf("The server %s, %d already exists.\n", ServerSettings{1,1}, ServerSettings{1,2});
end