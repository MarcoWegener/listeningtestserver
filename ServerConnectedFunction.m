function ServerConnectedFunction(src, ~)
%ServerConnectedFunction Called when connection status is changed
if src.Connected
    fprintf("Client has connected.\n");
else
    fprintf("Client has disconnected.\n");
end